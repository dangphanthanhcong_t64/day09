<?php
session_start();
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style_quiz.css">
    <title>Quiz</title>
</head>

<body>
    <main>
        <form action="quiz2.php" method="post" enctype="multipart/form-data">
            <div class="container">
                <?php
                // print_r($_SESSION["quiz_data"]);

                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    for ($i = 6; $i <= 10; $i++) {
                        $j = $i - 1;
                        if (isset($_POST["question$i"]) && !empty($_POST["question$i"])) {
                            setcookie("answer_user[$j]", $_POST["question$i"]);
                        } else {
                            setcookie("answer_user[$j]", "E");
                        }
                    }

                    // ksort($_COOKIE["answer_user"]);
                    // print_r($_COOKIE["answer_user"]);

                    header("location: evaluate.php");
                    exit();
                }

                for ($i = 6; $i <= 10; $i++) {
                    $j = $i - 1;
                    $question = $_SESSION["quiz_data"][$j]["question"];
                    $option_A = $_SESSION["quiz_data"][$j]["option"]["A"];
                    $option_B = $_SESSION["quiz_data"][$j]["option"]["B"];
                    $option_C = $_SESSION["quiz_data"][$j]["option"]["C"];
                    $option_D = $_SESSION["quiz_data"][$j]["option"]["D"];
                    echo "<div>
                    <label for='question$i'>Câu hỏi $i</label>$question</br>
                    <input type='radio' id='A' name='question$i' value='A'>A. $option_A<br>
                    <input type='radio' id='B' name='question$i' value='B'>B. $option_B<br>
                    <input type='radio' id='C' name='question$i' value='C'>C. $option_C<br>
                    <input type='radio' id='D' name='question$i' value='D'>D. $option_D<br>
                </div>";
                }
                ?>

                <button type="submit" class="button" name="evaluate">Nộp bài</button>
        </form>
    </main>
</body>

</html>