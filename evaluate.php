<?php
session_start();
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style_quiz.css">
    <title>Quiz</title>
</head>

<body>
    <main>
        <form action="evaluate.php" method="post" enctype="multipart/form-data">
            <div class="container">
                <?php
                // ksort($_COOKIE["answer_user"]);
                // print_r($_COOKIE["answer_user"]);

                $point = 0;
                for ($i = 0; $i < 10; $i++) {
                    if ($_COOKIE["answer_user"][$i] == $_SESSION["quiz_data"][$i]["answer"]) $point++;
                }
                echo "<div>Số điểm của bạn là: <label for='point'>$point/10</label></br> </div>";

                if ($point < 4) {
                    $comment = "Bạn quá kém, cần ôn tập thêm";
                } elseif ($point < 8) {
                    $comment = "Cũng bình thường";
                } else {
                    $comment = "Sắp sửa làm được trợ giảng lớp PHP";
                }
                echo "<font color='red'>$comment.</font> </br>";

                for ($i = 1; $i <= 10; $i++) {
                    $j = $i - 1;

                    $option = array("A", "B", "C", "D");
                    $font_color = array(
                        "A" => "black",
                        "B" => "black",
                        "C" => "black",
                        "D" => "black"
                    );
                    if ($_COOKIE["answer_user"][$j] == "E") {
                        $font_color["A"] = "red";
                        $font_color["B"] = "red";
                        $font_color["C"] = "red";
                        $font_color["D"] = "red";
                        $ans_real = $_SESSION["quiz_data"][$j]["answer"];
                        $font_color["$ans_real"] = "green";
                        $check = "✗. Sai";
                    } elseif ($_COOKIE["answer_user"][$j] == $_SESSION["quiz_data"][$j]["answer"]) {
                        $ans = $_COOKIE["answer_user"][$j];
                        $font_color["$ans"] = "green";
                        $check = "✓. Đúng";
                    } else {
                        $ans_user = $_COOKIE["answer_user"][$j];
                        $ans_real = $_SESSION["quiz_data"][$j]["answer"];
                        $font_color["$ans_user"] = "red";
                        $font_color["$ans_real"] = "green";
                        $check = "✗. Sai";
                    }

                    $question = $_SESSION["quiz_data"][$j]["question"];
                    $option_A = $_SESSION["quiz_data"][$j]["option"]["A"];
                    $option_B = $_SESSION["quiz_data"][$j]["option"]["B"];
                    $option_C = $_SESSION["quiz_data"][$j]["option"]["C"];
                    $option_D = $_SESSION["quiz_data"][$j]["option"]["D"];
                    $font_color_A = $font_color["A"];
                    $font_color_B = $font_color["B"];
                    $font_color_C = $font_color["C"];
                    $font_color_D = $font_color["D"];

                    echo "<div>
                        <label for='question$i'>Câu hỏi $i<br> $check</label>$question</br>
                        <font color=$font_color_A>A. $option_A</font> <br>
                        <font color=$font_color_B>B. $option_B</font> <br>
                        <font color=$font_color_C>C. $option_C</font> <br>
                        <font color=$font_color_D>D. $option_D</font> <br>
                    </div>";
                }
                ?>
        </form>
    </main>
</body>

</html>